import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  constructor(
    private _apiService: ApiService
  ) { }

  ngOnInit() {
    this.getAllMovies();
  }

  movieList = [];
  getAllMovies() {
    this._apiService.getAllMovies().subscribe(
      (res) => {
        this.movieList = res['Search'];
      }, (err) => { }
    );
  }

}
