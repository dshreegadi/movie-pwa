import { Injectable } from '@angular/core';
import { CONSTANTS } from './globals';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  getAllMovies() {
    const url = `https://www.omdbapi.com/?s=spider/&apikey=${CONSTANTS.OMDB_API_KEY}`;
    return this.http.get(url);
  }

  
}
